pdf:
	latexmk

clean:
	latexmk -c

clean-all:
	latexmk -C
	rm -rf build/

## Stuff to set up repository after cloning



.PHONY: pdf, clean, clean-all
