set -e
echo "Building document"
make pdf
mkdir public
mv build/Matboj_Regelwerk.pdf public
mv build/Matboj_Regelwerk.log public
cd public/
if ! command -v tree &> /dev/null
then
  echo "No tree utility found, skipping making tree"
else
  tree -H '.' -I "index.html" -D --charset utf-8 -T "$course" > index.html
fi

